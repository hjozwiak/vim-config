# Setup for the vim configuration
mv nvim ~/.config
cd ~/.config/nvim
mkdir -p pack/minpac/opt
cd pack/minpac/opt
git clone https://github.com/k-takata/minpac
