    
	scriptencoding utf-8
	set history=1000  				" Store a ton of history (default is 20)
	set spell 		 	        	" spell checking on
	" Setting up the directories {
		set backup 						" backups are nice ...
		set undoreload=10000 "maximum number lines to save for undo on a buffer reload
		set backupdir=$HOME/.config/nvim/backups//  " but not when they clog .
		set directory=$HOME/.config/nvim/swap// 	" Same for swap files
		set viewdir=$HOME/config/.nvim/views// 	" same for view files
	set ignorecase					" case insensitive search
	set smartcase					" case sensitive when uc present
	set gdefault					" the /g flag on :s substitutions by default
	set wrap                     	" wrap long lines
    set noruler
	set formatoptions+=l
	set lbr
	set shiftwidth=4               	" use indents of 4 spaces
	"set expandtab 	  	     		" tabs are spaces, not tabs
	set tabstop=4 					" an indentation every four columns
	set softtabstop=4 				" let backspace delete indent
	let mapleader = ','
    nnoremap ; :

	" Yank from the cursor to the end of the line, to be consistent with C and D.
	nnoremap Y y$
		
	" Shortcuts
	" Change Working Directory to that of the current file
    cmap cwd lcd %:p:h
	cmap cd. lcd %:p:h

	" For when you forget to sudo.. Really Write the file.
	cmap w!! w !sudo tee % >/dev/null
" }


filetype plugin indent on  " allows auto-indenting depending on file type




packadd minpac
call minpac#init()
call minpac#add('jreybert/vimagit')
call minpac#add('k-takata/minpac', {'type': 'opt'})
call minpac#add('tpope/vim-unimpaired')
call minpac#add('scrooloose/nerdtree')
call minpac#add('tpope/vim-dispatch')
call minpac#add('tpope/vim-projectionist')
call minpac#add('radenling/vim-dispatch-neovim')
call minpac#add('w0rp/ale')
call minpac#add('fatih/vim-go')
call minpac#add('lervag/vimtex')
call minpac#add('SirVer/ultisnips')

call minpac#add ('airblade/vim-gitgutter')
call minpac#add ('vim-scripts/grep.vim')
call minpac#add ('vim-scripts/CSApprox')
call minpac#add ('bronson/vim-trailing-whitespace')
call minpac#add ('Raimondi/delimitMate')
call minpac#add ('majutsushi/tagbar')
call minpac#add ('scrooloose/syntastic')
call minpac#add ('Yggdroot/indentLine')

call minpac#add( 'sheerun/vim-polyglot')




call minpac#add( 'honza/vim-snippets')

"" Color
call minpac#add( 'tomasr/molokai')

"*****************************************************************************
"" Custom bundles
"*****************************************************************************

" c
call minpac#add( 'vim-scripts/c.vim')
call minpac#add( 'ludwig/split-manpage.vim')





" haskell
"" Haskell Bundle
call minpac#add( 'eagletmt/neco-ghc')
call minpac#add( 'dag/vim2hs')
call minpac#add( 'pbrisbin/vim-syntax-shakespeare')


" html
"" HTML Bundle
call minpac#add( 'hail2u/vim-css3-syntax')
call minpac#add( 'gorodinskiy/vim-coloresque')
call minpac#add( 'tpope/vim-haml')
call minpac#add( 'mattn/emmet-vim')


" javascript
"" Javascript Bundle
call minpac#add( 'jelera/vim-javascript-syntax')


" lisp
"" Lisp Bundle
call minpac#add( 'vim-scripts/slimv.vim')


" perl
"" Perl Bundle
call minpac#add( 'vim-perl/vim-perl')
call minpac#add( 'c9s/perlomni.vim')


" php
"" PHP Bundle
call minpac#add( 'arnaud-lb/vim-php-namespace')


" python
"" Python Bundle
call minpac#add( 'davidhalter/jedi-vim')
call minpac#add( 'raimon49/requirements.txt.vim')


" ruby
call minpac#add( 'tpope/vim-rails')
call minpac#add( 'tpope/vim-rake')
call minpac#add( 'tpope/vim-projectionist')
call minpac#add( 'thoughtbot/vim-rspec')
call minpac#add( 'ecomba/vim-ruby-refactoring')


" rust
" Vim racer
call minpac#add( 'racer-rust/vim-racer')

" Rust.vim
call minpac#add( 'rust-lang/rust.vim')




packloadall
command! PackUpdate call minpac#update()
command! PackClean call minpac#clean()






















